require "telegramAPI"

TOKEN_TELEGRAM    = ENV["TOKEN_TELEGRAM"] || '544582112:AAFvV4f9dcc2giBOAn1cYUQNGwfpTEPVoZ0'
CHANNEL_TELEGRAM  = ENV["CHANNEL_TELEGRAM"] || "@mylogger"
MESSAGE_AUTHOR    = ENV["MESSAGE_AUTHOR"] || "cosmos404"
MINIMUM_LENGTH    = ENV["MINIMUM_LENGTH"] || 200
MODE              = ENV["MODE"] || "FORWARD" # or "MESSAGE"

@bot = TelegramAPI.new(TOKEN_TELEGRAM)

bot_info = @bot.getMe()
puts "Hello! My name is #{bot_info["first_name"]}!"
puts "My id is #{bot_info["id"]}."
puts "And my username is @#{bot_info["username"]}."

def message_mode(message)
  msg_id        = message["message_id"]
  chat_username = message["chat"]["username"]
  message_link  = "https://t.me/#{chat_username}/#{msg_id}"
  message_link  = "<a href='#{message_link}'>Оригинал</a>"
  msg           = message["text"]

  @bot.sendMessage( CHANNEL_TELEGRAM,
                    "#{msg}\n#{message_link}",
                    { parse_mode: "HTML", disable_web_page_preview: true })
end

def forward_mode(message)
    msg_id    = message["message_id"]
    chat_id   = message["chat"]["id"]
    @bot.forwardMessage(CHANNEL_TELEGRAM, chat_id, msg_id)
end

while true do
  updates = @bot.getUpdates({"timeout"=>180})

  updates.each do |message|
    next if not message["channel_post"].nil?
    next if message["message"].nil?
    next if not message["message"]["forward_from"].nil?

    message   = message["message"]
    username  = message["from"]["username"]
    msg       = message["text"]

    if not msg.nil?
      p Time.now.strftime("%H:%M %d/%m/%Y @#{username}: #{msg.length}")
      next if msg.length < MINIMUM_LENGTH.to_i
    end
    next if username != MESSAGE_AUTHOR

    case MODE.upcase
    when "MESSAGE"
      message_mode(message)
    when "FORWARD"
      forward_mode(message)
    end
  end
end
