# RepeaTolik
_Бот для пересылки соообщений из чатов на канал._

#### Для запуска необходимы:
- Ruby 2.4+

#### Для старта:
- bundle install
- ruby app.rb

#### Переменные среды:
- TOKEN_TELEGRAM(required) — токен телеграм бота
- CHANNEL_TELEGRAM(required) — телеграм канал куда отправлять сообщения(Прим: @channel)
- MESSAGE_AUTHOR(required) — Автор, чьи сообщения отправлять на канал(Прим: username, без @)
- MINIMUM_LENGTH(default: 200) — Минимальная длина для сообщений.
- MODE(default: MESSAGE) — Мод отправки сообщений. MESSAGE — отправляет сообщение в конце которого вставляет ссылку на оригинал. FORWARD — делает форварды сообщений.
